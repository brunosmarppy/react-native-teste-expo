/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 */

import React from 'react';
import {
  Button,
  SafeAreaView,
} from 'react-native';
import FileViewer from 'react-native-file-viewer';
import * as FileSystem from 'expo-file-system';

function App() {

  downloadFile = async () => {
    const url = "https://www.africau.edu/images/default/sample.pdf";

    try {
      this.downloadProcess = FileSystem.createDownloadResumable(
        url,
        `${FileSystem.documentDirectory}document.pdf`,
        {},
      );

      const file = await this.downloadProcess.downloadAsync();
      console.log(file);

      await FileViewer.open(file.uri);
    } catch (error) {
      console.warn(error);
    }
  }

  return (
    <SafeAreaView>
      <Button onPress={this.downloadFile} title="Press me" />
    </SafeAreaView>
  );
}

export default App;
